Model_and_Properties contains the model file and the property file.
Prism must be installed to perform simulation or model checking on it.

Scripts contains a shell script and some python code. By running the shell script,
you will run a Prism experiment using the simulation method.
    runPrismExperiment.sh must modified at line 102 and 118 depending on where you cloned the repo.
    create_and_analyse_data.py must be modified at line 26 depending on where you cloned the repo.

To run the script, write in your console:
    ./runPrismExperiment.sh modelFileName propertyFileName idProperty quantityOfExperiments
where :
    modelFileName = Path of the model's file without extension.
    propertyFileName = Path of the properties file without extension.
            (If the path is the same as the model, use keyword "idem".)
    idProperty = Id number of the wanted property (position in the list of properties)
    quantityOfExperiments = Size of the wanted sample.

The script will then ask you the name of the directory you want to create to store data and the shared name of the results.
