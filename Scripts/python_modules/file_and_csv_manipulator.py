#!/usr/bin/env python

"""
Handle files and data frames from Run_experiment.shself.

Several Classes are stored here. FileManipulators aim to
open and save file given path, name and even quantity.

DataFrameManipulator aims to create a Dataframe out of the
result of Run_experiment.sh

"""

from collections import OrderedDict
from matplotlib import pyplot as plt
import pandas as pd


class FileManipulator(object):
    """Parent class with information to open/save a file."""

    def __init__(self, folder, used_file):
        """Store basic attributes to open/save data."""
        self.file_name = used_file
        self.folder_name = folder
        self.file_path = folder+"/"+used_file

    def opener(self):
        """Overrride this method."""
        print "This class is not prepared to open " +\
            self.file_name

    def saver(self):
        """Overrride this method."""
        print "This class is not prepared to save " +\
            self.file_name


class CsvManipulator(FileManipulator):
    """Open and save .txt file containing csv format."""

    def __init__(self, folder, used_file, quantity=1):
        """Add a quantity to the attribute if there is."""
        FileManipulator.__init__(self, folder, used_file)
        self.file_number = quantity
        self.data_frame_to_save = []
        self.name_details_data_frame = []

    def opener(self):
        """Open all csv files with .txt extension."""
        data_frames = {}
        for i in range(1, self.file_number+1):
            real_file_name = self.file_path+str(i)+".txt"
            data_frames[i] = pd.read_csv(
                real_file_name, sep=',')
        return data_frames

    def add_data_frame_and_detail(self,
                                  data_frames,
                                  name_details):
        """Allow to add these necessary data to save."""
        self.data_frame_to_save = data_frames
        self.name_details_data_frame = name_details

    def saver(self):
        """Save a csv file with .txt extension."""
        index_list = 0
        while index_list < len(self.data_frame_to_save):
            data_frame_path = self.folder_name + "/" \
                + self.file_name\
                + self.name_details_data_frame[index_list]\
                + "_Concat.txt"
            self.data_frame_to_save[index_list].to_csv(
                data_frame_path, index=False)
            index_list += 1


class PlotManipulator(FileManipulator):
    """Save plots under .png extension."""

    def __init__(self, folder, used_file, plot_to_manipulate):
        """Add manipulated_plot to the attributes."""
        FileManipulator.__init__(
            self, folder+"/Figures", used_file)
        self.manipulated_plot = plot_to_manipulate

    def saver(self):
        """Save a plot figure with .png extension."""
        fig_to_save = self.manipulated_plot.get_figure()
        fig_to_save.set_size_inches(15, 4.5)
        fig_to_save.tight_layout()
        figure_path = self.folder_name + "/"\
            + self.file_name + ".png"
        fig_to_save.savefig(figure_path, dpi=plt.gcf().dpi)


class TxtManipulator(FileManipulator):
    """Open and save .txt file containing plain text."""

    def __init__(self, folder, used_file, text_to_write):
        """
        Add text_file to the attributes.

        Will open, write and save by itself.
        """
        FileManipulator.__init__(self, folder, used_file)
        self.text_file = open(self.file_path+'.txt', 'w')
        self.text_file.write(text_to_write)
        self.text_file.close()
        print self.file_path+'.txt has been saved'


class DataFrameManipulator(object):
    """Create a data frame with the given dataframes ."""

    def __init__(self):
        """Create only attribute as an empty data frame."""
        self.raw_frame = None
        self.create_blank_df()

    def create_blank_df(self):
        """Create and return an empty data frame."""
        setup_dict = OrderedDict()
        setup_dict["step"] = 'null'
        self.raw_frame = pd.DataFrame(
            setup_dict,
            columns=setup_dict.keys(),
            index=[0])

    def add_data_to_my_frame(self, frame_to_add):
        """
        Concatenate a given data frame in self.raw_frameself.

        If self.raw_frame is empty, this method is giving it
        the right size to concatenate with the give
        dataframe.
        """
        result_name = list(frame_to_add.head(0))[1]
        if self.raw_frame.step[0] == 'null':
            update_length = []
            for _ in range(1, len(frame_to_add.i[0:])):
                update_length += [['null']]
            self.raw_frame = self.raw_frame.append(
                pd.DataFrame(
                    update_length, columns=['step']))
            self.raw_frame = self.raw_frame.reset_index(
                drop=True)
            self.raw_frame.step = frame_to_add.i[0:]
            first_data = frame_to_add[result_name].tolist()
            self.raw_frame.insert(
                1, "Experiment 1", first_data)
        else:
            other_data = frame_to_add[result_name].tolist()
            column_index = len(self.raw_frame.columns)
            column_name = "Experiment "+str(column_index)
            self.raw_frame.insert(column_index, column_name, other_data)

    def return_data_frame(self):
        """Return self.raw_frame to user."""
        return self.raw_frame
