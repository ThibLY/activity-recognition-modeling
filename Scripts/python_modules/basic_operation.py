#!/usr/bin/env python

"""
In this module, You can find complementary functions.

These functions are only used by analyse_statistics.py.
"""


def biggest_divisor(num, first_divider, second_divider):
    """Return the biggest divisor of num."""
    # if divisible by first_divider
    if num % first_divider == 0:
        return num/first_divider
    # iterate from second_divider to sqrt(n)
    i = second_divider
    while i * i <= num:
        if num % i == 0:
            return num/i
        i += 2
    return num


def round_to_multiple(num, mult):
    """Return the multiple of m nearest to n and higher."""
    ans = (num//mult)*(mult)
    return ans


def set_top_tick_granul_loop(top_tick, value_to_round_to):
    """Return 3 values used by tick_granularity()."""
    top_tick = round_to_multiple(top_tick, value_to_round_to)
    tick_granul = (top_tick/value_to_round_to)+1
    continue_loop = False
    return (top_tick, tick_granul, continue_loop)


def tick_granularity(top_tick):
    """
    Calculate tick_granularity and reset top_tick if needed.

    Reset is only done if we are not able to have a good
    tick readability for the figure.
    """
    continue_loop = True
    tick_granul = 0
    while continue_loop:
        if top_tick/20 <= 1:
            # if low amount of ticks, one grid line per tick
            tick_granul = top_tick+1
            continue_loop = False
        elif top_tick/40 <= 1:
            # if more, one grid line per 2 ticks or less
            tick_granul = biggest_divisor(
                top_tick, 2, 3)
            if tick_granul == top_tick:
                top_tick = round_to_multiple(top_tick, 5)
                # reset tick to a higher easier value
            else:
                tick_granul += 1
                continue_loop = False
        elif top_tick/60 <= 1:
            # if even more, one grid line per 4 ticks max
            tick_granul = biggest_divisor(
                top_tick, 4, 5)
            if tick_granul == top_tick:
                (
                    top_tick,
                    tick_granul,
                    continue_loop
                ) = set_top_tick_granul_loop(
                    top_tick, 10)
            else:
                tick_granul += 1
                continue_loop = False
        elif top_tick/150 <= 1:
            # if many, one grid line per 10 ticks max
            (
                top_tick,
                tick_granul,
                continue_loop
            ) = set_top_tick_granul_loop(top_tick, 10)
        elif top_tick/500 <= 1:
            # if many more, one grid line per 50 ticks max
            (
                top_tick,
                tick_granul,
                continue_loop
            ) = set_top_tick_granul_loop(top_tick, 50)
        else:
            # if too many, one grid line per 100 ticks max
            (
                top_tick,
                tick_granul,
                continue_loop
            ) = set_top_tick_granul_loop(top_tick, 100)
    return top_tick, tick_granul
